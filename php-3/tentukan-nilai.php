<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tentukan Nilai</title>
</head>
<body>
    <?php
        function tentukan_nilai($number)
        {
            if($number >= 85 && $number <= 100){
                $hasil = "Sangat Baik";
            } 
            else if($number >= 70 && $number < 85){
                $hasil = "Baik";
            }
            else if($number >= 60 && $number < 70){
                $hasil = "Cukup";
            }
            else{
               $hasil = "Kurang";
            }
            return $hasil . "<br>";
        }

        //TEST CASES
        echo tentukan_nilai(98); //Sangat Baik
        echo tentukan_nilai(76); //Baik
        echo tentukan_nilai(67); //Cukup
        echo tentukan_nilai(43); //Kurang
    ?>
</body>
</html>