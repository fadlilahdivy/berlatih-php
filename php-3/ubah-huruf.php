<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ubah Huruf</title>
</head>
<body>
    <?php
        function ubah_huruf($string){
            $abjad = ['a','b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                      'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
                      'u', 'v', 'w', 'x', 'y', 'z'];
            $kata = str_split($string);
            $newstring = '';

           for($i = 0 ; $i <= count($kata)-1; $i++)
           {
               for($j = 0; $j < count($abjad); $j++){
                    if($kata[$i] == $abjad[$j]){
                        $newstring .= $abjad[$j+1];
                    }
               } 
           }
           return $newstring . "<br>";
        }
        
        // TEST CASES
        echo ubah_huruf('wow'); // xpx
        echo ubah_huruf('developer'); // efwfmpqfs
        echo ubah_huruf('laravel'); // mbsbwfm
        echo ubah_huruf('keren'); // lfsfo
        echo ubah_huruf('semangat'); // tfnbohbu
    ?>
</body>
</html>