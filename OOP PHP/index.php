<?php

    require_once('Animal.php');
    require_once('Ape.php');
    require_once('Frog.php');

    $sheep = new Animal("shaun");
        echo "Name Animals: $sheep->name <br>"; // "shaun"
        echo "Legs is: $sheep->legs <br>"; // 2
        echo "The Animal is Cold Blooded: $sheep->cold_blooded <br> <br>"; // false

    $sungokong = new Ape("kera sakti");
        echo "Name Animals: $sungokong->name <br>"; // "kera sakti"
        echo "Legs is: $sungokong->legs <br>"; // 2
        echo "The Animal is Cold Blooded: $sungokong->cold_blooded <br>"; // false
        echo $sungokong->yell(); // "Auooo"

    $kodok = new Frog("buduk");
        echo "Name Animals: $kodok->name <br>"; // "buduk"
        echo "Legs is: $kodok->legs <br>"; // 4
        echo "The Animal is Cold Blooded: $kodok->cold_blooded <br>"; // true
        echo $kodok->jump() //"hop hop"
?>
